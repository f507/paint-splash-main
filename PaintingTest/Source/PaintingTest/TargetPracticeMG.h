// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MiniGameBase.h"
#include "MovableDynamicWorldObjects.h"
#include "TargetPracticeMG.generated.h"

/**
 * 
 */
UCLASS()
class PAINTINGTEST_API ATargetPracticeMG : public AMiniGameBase
{
	GENERATED_BODY()
	
public:
	ATargetPracticeMG();

protected:

	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Setup")
	TArray<AMovableDynamicWorldObjects*> m_Targets;


public:

	virtual void Tick(float DeltaTime) override;
};
