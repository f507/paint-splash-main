// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PaintingTestGameMode.generated.h"

UCLASS(minimalapi)
class APaintingTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APaintingTestGameMode();
};



