// Fill out your copyright notice in the Description page of Project Settings.


#include "MiniGameManager.h"
#include "MiniGameBase.h"
// Sets default values
AMiniGameManager::AMiniGameManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMiniGameManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMiniGameManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

