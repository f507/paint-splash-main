// Fill out your copyright notice in the Description page of Project Settings.


#include "GreenPaint.h"

UGreenPaint::UGreenPaint()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UGreenPaint::BeginPlay()
{
	Super::BeginPlay();

	//Setup owning paintable actor if any
	m_OwningPaintableActor = GetOwner();

	if (m_OwningPaintableActor)
	{
		m_InitialActorScale = m_MinScale == FVector::OneVector || m_MinScale == FVector::ZeroVector? m_OwningPaintableActor->GetActorScale3D() : m_MinScale;
		m_ActorMaxScale = m_InitialActorScale * m_MaxScale;
	}
}
void UGreenPaint::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//Don't tick if the same colored paint was not applied to the owning actor
	if (!b_ColorApplied) return;
	
	

	InitializePaintScaling(DeltaTime);
	ScaleFromPaintApplication(m_ScalePercentRate);
	UpdateFMODVariables();
}

void UGreenPaint::ResetColorAttrs()
{
	//Temporary reset TODO reset the actor back to initial state before paint was applied
	m_AlphaGrowingDirection = -1;
}

void UGreenPaint::ApplyPaintAttrs()
{
	Super::ApplyPaintAttrs();
	b_ColorApplied = true;
	m_AlphaGrowingDirection = 1;
}

void UGreenPaint::OnColorMix()
{
}

void UGreenPaint::ScaleFromPaintApplication(float ScaleRate)
{
	if (!m_OwningPaintableActor) return;

	m_CurrentScaleAlpha += ScaleRate;

	m_CurrentScaleAlpha = FMath::Clamp(m_CurrentScaleAlpha, 0.f, 1.f);

	m_OwningPaintableActor->SetActorScale3D(FMath::Lerp(m_InitialActorScale, m_MaxScale, m_CurrentScaleAlpha));
	
	if (m_CurrentScaleAlpha == 0) b_ColorApplied = false;
	//SetActorRelativeScale3D(FVector(2.f,2.f,2.f));
	//m_Root->SetWorldScale3D(FVector(2.f, 2.f, 2.f));
}

void UGreenPaint::UpdateFMODVariables() {
	m_Growing = m_ScalePercentRate > 0 && m_CurrentScaleAlpha < 1.f;
	m_Shrinking = m_ScalePercentRate < 0 && m_CurrentScaleAlpha > 0.f;
}

void UGreenPaint::InitializePaintScaling(float DeltaTime)
{
	m_ScalePercentRate = (1 / m_ScaleToMaxTime) * DeltaTime;

	m_ScalePercentRate *= m_AlphaGrowingDirection;
}