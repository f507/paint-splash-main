// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MiniGameManager.generated.h"

UENUM()
enum EGames
{
	INACTIVE,
	TARGET_PRACTICE  UMETA(DisplayName = "Target Practice"),
	PAINT_BOWLING    UMETA(DisplayName = "Bowling")
};


UCLASS()
class PAINTINGTEST_API AMiniGameManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMiniGameManager();

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Games")
	TEnumAsByte<EGames> CurrentActiveGame;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Games")
	TArray<class AMiniGameBase*> GamesToTrackInLevel;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	inline EGames GetCurrentActiveGame() { return CurrentActiveGame; }
};
