// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PaintColor.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PAINTINGTEST_API UPaintColor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPaintColor();

	
protected:
	//TODO use smart pointers, preferably TUniquePtr<>
	UPROPERTY()
	AActor* m_OwningPaintableActor = nullptr;
	UPROPERTY(VisibleAnywhere)
	bool b_ColorApplied = false;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	//Methods to be overiden per each color
	virtual void ResetColorAttrs();
	virtual void ApplyPaintAttrs();
	virtual void OnColorMix();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
