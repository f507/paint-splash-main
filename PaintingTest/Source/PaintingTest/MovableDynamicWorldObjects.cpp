 // Fill out your copyright notice in the Description page of Project Settings.


#include "MovableDynamicWorldObjects.h"

#include "Engine.h"

#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "Particles/ParticleSystem.h"

//Color Comps
#include "PaintColor.h"
#include "YellowPaint.h"
#include "GreenPaint.h"
#include "BluePaint.h"
#include "RedPaint.h"

// Sets default values
AMovableDynamicWorldObjects::AMovableDynamicWorldObjects()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_Root = GetRootComponent();
	SetRootComponent(m_Root);

	m_MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	m_MeshComp->SetupAttachment(m_Root);

	/*m_BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Box"));
	m_BoxComp->SetupAttachment(m_Root);*/

	m_YellowPaintComp = CreateDefaultSubobject<UYellowPaint>(TEXT("Yellow Component"));
	m_BluePaintComp = CreateDefaultSubobject<UBluePaint>(TEXT("Blue Component"));
	m_GreenPaintComp = CreateDefaultSubobject<UGreenPaint>(TEXT("Green Component"));

	m_psComp = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle"));
	m_psComp->SetupAttachment(m_Root);
	

	static ConstructorHelpers::FObjectFinder<UMaterial> PaintableMaterial(TEXT("Material'/Game/Materials/M_TempColorSwap.M_TempColorSwap'"));
	if (PaintableMaterial.Succeeded())
	{
		m_PaintableMaterial = (UMaterial*)PaintableMaterial.Object;
		//m_MeshComp->SetMaterial(0, m_PaintableMaterial);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Couldn't find material"))
	}

	/// <summary>
	/// Dynamic Material References
	/// </summary>
	static ConstructorHelpers::FObjectFinder<UMaterial> PaintableBlue(TEXT("Material'/Game/Materials/M_DynamicBlue.M_DynamicBlue'"));
	if (PaintableBlue.Succeeded())
	{
		m_DynamicMaterials.Blue = (UMaterial*)PaintableBlue.Object;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Couldn't find Blue material"))
	}

	static ConstructorHelpers::FObjectFinder<UMaterial> PaintableYellow(TEXT("Material'/Game/Materials/M_DynamicYellow.M_DynamicYellow'"));
	if (PaintableBlue.Succeeded())
	{
		m_DynamicMaterials.Yellow = (UMaterial*)PaintableYellow.Object;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Couldn't find Yellow material"))
	}

	static ConstructorHelpers::FObjectFinder<UMaterial> PaintableGreen(TEXT("Material'/Game/Materials/M_DynamicGreen.M_DynamicGreen'"));
	if (PaintableBlue.Succeeded())
	{
		m_DynamicMaterials.Green = (UMaterial*)PaintableGreen.Object;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Couldn't find Green material"))
	}

	static ConstructorHelpers::FObjectFinder<UMaterial> PaintableWhite(TEXT("Material'/Game/Materials/M_DynamicWhite.M_DynamicWhite'"));
	if (PaintableBlue.Succeeded())
	{
		m_DynamicMaterials.White = (UMaterial*)PaintableWhite.Object;
		m_MeshComp->SetMaterial(0, m_DynamicMaterials.White);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Couldn't find White material"))
	}

	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleSystem(TEXT("ParticleSystem'/Game/VFX/PS_PaintableObjectIndicator.PS_PaintableObjectIndicator'"));
	if (ParticleSystem.Succeeded())
	{
		m_ps = (UParticleSystem*)ParticleSystem.Object;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Couldn't find material"))
	}

	m_psComp->SetTemplate(m_ps);
}

// Called when the game starts or when spawned
void AMovableDynamicWorldObjects::BeginPlay()
{
	Super::BeginPlay();

	
	/*m_YellowPaintComp = FindComponentByClass<UYellowPaint>();
	m_GreenPaintComp = FindComponentByClass<UGreenPaint>();
	m_BluePaintComp = FindComponentByClass<UBluePaint>();*/

	m_PreviousColorApplied = ColorsApplied::NONE;
	m_ColorApplied = ColorsApplied::NONE;
	m_LastColorApplied = ColorsApplied::NONE;

	FTransform particleTransform;
	particleTransform.SetLocation(GetActorLocation());
	const FTransform& pTransform = particleTransform;
	//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), m_ps, pTransform,true,EPSCPoolMethod::None,true);
	if (m_psComp)
	{
		m_psComp = UGameplayStatics::SpawnEmitterAtLocation((const UObject*)GetWorld(), m_ps, GetActorLocation(), GetActorRotation(), FVector(1, 1, 1), true, EPSCPoolMethod::None, true);
		m_psComp->bHiddenInGame = true;
	}
	
}

void AMovableDynamicWorldObjects::OnColorChange()
{
	
}


void AMovableDynamicWorldObjects::ResetActorToDefaultState()
{
	TArray<UPaintColor*> colorComponents = { (UPaintColor*)m_YellowPaintComp, (UPaintColor*)m_GreenPaintComp, (UPaintColor*)m_BluePaintComp };

	for (UPaintColor* colorComp : colorComponents)
	{
		if(colorComp != nullptr)
		{
			colorComp->ResetColorAttrs();
		}
	}
}

void AMovableDynamicWorldObjects::OnPaintedGreen()
{
	m_GreenPaintComp->ApplyPaintAttrs();
	if (m_BluePaintComp) m_BluePaintComp->ResetColorAttrs();
	if (m_YellowPaintComp) m_YellowPaintComp->ResetColorAttrs();
}

void AMovableDynamicWorldObjects::OnPaintedYellow()
{
}

void AMovableDynamicWorldObjects::OnPaintedBlue()
{
}

// Called every frame
void AMovableDynamicWorldObjects::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Only non-stationary actors need to Tick
	if (!m_bIsPaintable) return;

	bool b_delayDone = UGameplayStatics::GetTimeSeconds(GetWorld()) >= m_PaintDelayHandler;

	if (m_ColorApplied != ColorsApplied::NONE)
	{
		if (m_YellowPaintComp)
		{
			if (m_ColorApplied == ColorsApplied::YELLOW) {

				m_YellowPaintComp->ApplyPaintAttrs();
			}
		}

		if (m_GreenPaintComp)
		{
			if (m_ColorApplied == ColorsApplied::GREEN) 
			{ 
				OnPaintedGreen();
			}

		}


		if (m_BluePaintComp)
		{

			if (m_ColorApplied == ColorsApplied::BLUE)
			{

				m_BluePaintComp->ApplyPaintAttrs();
			}
		}
		else
		{
			//UE_LOG(LogTemp, Warning, TEXT("NO BLUE"))
		}

		if (m_ColorApplied == ColorsApplied::WHITE)
		{
			ResetActorToDefaultState();
		}
		
		m_LastColorApplied = m_ColorApplied;


		//FString result = FString::FromInt(m_ColorApplied);
		//UE_LOG(LogTemp, Warning, TEXT("%s: current color"), *result)
		//UE_LOG(LogTemp, Warning, TEXT("%s: previous color"), *FString::FromInt(m_PreviousColorApplied))

		//Activate color functionality only once to avoid infinite loops
		m_ColorApplied = ColorsApplied::NONE;
	}
	
	

}

void AMovableDynamicWorldObjects::SetColorApplied(uint8 colorID)
{
	float currentGameTime = UGameplayStatics::GetTimeSeconds(GetWorld());
	bool b_delayDone = currentGameTime >= m_PaintDelayHandler;
	if (!b_delayDone) return;

	m_PreviousColorApplied = m_LastColorApplied;

	

	//UE_LOG(LogTemp, Warning, TEXT("%s: previous color"), *FString::FromInt(m_PreviousColorApplied))
	//TODO include color enum to simply assign the passed color to the color applied property
	bool IsCurrentlyMixedColor = m_PreviousColorApplied == ColorsApplied::GREEN || m_PreviousColorApplied == ColorsApplied::ORANGE || m_PreviousColorApplied == ColorsApplied::PURPLE;
	bool IsWhiteApplied = colorID == 7;

	if (!IsCurrentlyMixedColor)
	{
		switch (colorID)
		{
		case 0:
			//to test scaling for now I will switch red color for green
			m_ColorApplied = ColorsApplied::RED;
			break;
		case 1:
			m_ColorApplied = ColorsApplied::YELLOW;
			break;
		case 2:
			m_ColorApplied = ColorsApplied::BLUE;
			break;
		case 7:
			m_ColorApplied = ColorsApplied::WHITE;
			break;
		default:
			m_ColorApplied = ColorsApplied::NONE;
			break;
		}
	}

	if (m_PreviousColorApplied == ColorsApplied::GREEN && colorID == 7) 
	{
		m_ColorApplied = ColorsApplied::WHITE;
	}

	///Check for potential color mixing
	//TODO current solution is brute-forced, a more modular and scalable solution is recommended

	if (m_PreviousColorApplied == ColorsApplied::RED && m_ColorApplied != ColorsApplied::RED)
	{
		bool shouldMakeOrange = m_ColorApplied == ColorsApplied::YELLOW;
		bool shouldMakePurple = m_ColorApplied == ColorsApplied::BLUE;

		
		if (shouldMakeOrange) { m_ColorApplied = ColorsApplied::ORANGE; }
		
		 if (shouldMakePurple) { m_ColorApplied = ColorsApplied::PURPLE; }
	}
	else if (m_PreviousColorApplied == ColorsApplied::YELLOW && m_ColorApplied != ColorsApplied::YELLOW)
	{
		bool shouldMakeOrange = m_ColorApplied == ColorsApplied::RED;
		bool shouldMakeGreen = m_ColorApplied == ColorsApplied::BLUE;

		if (shouldMakeOrange) 
		{ 
			m_ColorApplied = ColorsApplied::ORANGE; 
		}
		else if (shouldMakeGreen) 
		{ 
			m_ColorApplied = ColorsApplied::GREEN;
		}
	}
	else if (m_PreviousColorApplied == ColorsApplied::BLUE && m_ColorApplied != ColorsApplied::BLUE)
	{
		

		bool shouldMakePurple = m_ColorApplied == ColorsApplied::RED;
		bool shouldMakeGreen = m_ColorApplied == ColorsApplied::YELLOW;

		if (shouldMakePurple) { m_ColorApplied = ColorsApplied::PURPLE; }
		else if (shouldMakeGreen) {

			m_ColorApplied = ColorsApplied::GREEN;

			
		}
	}
	
}

void AMovableDynamicWorldObjects::SwapMaterials(UMaterial* newMaterial, uint8 ColorID)
{
	float currentGameTime = UGameplayStatics::GetTimeSeconds(GetWorld());
	bool b_delayDone = UGameplayStatics::GetTimeSeconds(GetWorld()) >= m_PaintDelayHandler;
	if (!newMaterial || !b_delayDone) return;

	bool IsCurrentlyMixedColor = m_PreviousColorApplied == ColorsApplied::GREEN || m_PreviousColorApplied == ColorsApplied::ORANGE || m_PreviousColorApplied == ColorsApplied::PURPLE;
	if (IsCurrentlyMixedColor && m_ColorApplied != ColorsApplied::WHITE)
	{
		return;
	}

	UMaterialInstanceDynamic* mat_o = UMaterialInstanceDynamic::Create(newMaterial, m_MeshComp);

	//TMap<uint8, FLinearColor> colorMap;
	TMap<uint8, UMaterial*> colorMap;
	//colorMap.Add(0, FLinearColor::Red);
	//colorMap.Add(1, FLinearColor::Yellow);
	//colorMap.Add(2, FLinearColor::Blue);
	//colorMap.Add(7, FLinearColor::White);
	 
	colorMap.Add(1, m_DynamicMaterials.Yellow);
	colorMap.Add(2, m_DynamicMaterials.Blue);
	colorMap.Add(7, m_DynamicMaterials.White);

	bool mixedColor = m_ColorApplied == ColorsApplied::GREEN || m_ColorApplied == ColorsApplied::ORANGE || m_ColorApplied == ColorsApplied::PURPLE;
	if (mixedColor)
	{
		/*if(m_ColorApplied == ColorsApplied::GREEN && m_GreenPaintComp)
		{
			mat_o->SetVectorParameterValue(TEXT("PaintedColor"), FLinearColor::Green);
			m_MeshComp->SetMaterial(0, mat_o);
		}
		else if (m_ColorApplied == ColorsApplied::ORANGE)
		{
			mat_o->SetVectorParameterValue(TEXT("PaintedColor"), FLinearColor(1.f, .3f,0.f));
			m_MeshComp->SetMaterial(0, mat_o);
		}
		else if (m_ColorApplied == ColorsApplied::PURPLE)
		{
			mat_o->SetVectorParameterValue(TEXT("PaintedColor"), FLinearColor(.5, 0.f, .5f));
			m_MeshComp->SetMaterial(0, mat_o);
		}*/

		if (m_ColorApplied == ColorsApplied::GREEN && m_GreenPaintComp)
		{
			m_MeshComp->SetMaterial(0, m_DynamicMaterials.Green);
		}

	}
	else if(m_YellowPaintComp && m_BluePaintComp)
	{
		//FLinearColor selectedColor = colorMap[ColorID];
		UMaterial* selectedColor = colorMap[ColorID];

		/*mat_o->SetVectorParameterValue(TEXT("PaintedColor"), selectedColor);
		m_MeshComp->SetMaterial(0, mat_o);*/
		
		m_MeshComp->SetMaterial(0, selectedColor);

		//UE_LOG(LogTemp, Warning, TEXT("Color selected: %s"), *selectedColor.ToString())
	}
	else
	{
		//FLinearColor selectedColor = colorMap[ColorID];
		UMaterial* selectedColor = colorMap[ColorID];
		/*if (ColorID == 1 && m_YellowPaintComp)
		{
			mat_o->SetVectorParameterValue(TEXT("PaintedColor"), selectedColor);
			m_MeshComp->SetMaterial(0, mat_o);
		}
		else if (ColorID == 2 && m_BluePaintComp)
		{
			mat_o->SetVectorParameterValue(TEXT("PaintedColor"), selectedColor);
			m_MeshComp->SetMaterial(0, mat_o);
		}
		else if (ColorID == 7)
		{
			m_MeshComp->SetMaterial(0, m_PaintableMaterial);
		}*/

		if (ColorID == 1 && m_YellowPaintComp)
		{
			m_MeshComp->SetMaterial(0, selectedColor);
		}
		else if (ColorID == 2 && m_BluePaintComp)
		{
			m_MeshComp->SetMaterial(0, selectedColor);
		}
		else if (ColorID == 7)
		{
			m_MeshComp->SetMaterial(0, selectedColor);
		}
	}
	
	m_PaintDelayHandler = currentGameTime + m_PaintAppDelay_InSec;

}

