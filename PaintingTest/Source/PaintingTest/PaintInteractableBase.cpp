// Fill out your copyright notice in the Description page of Project Settings.


#include "PaintInteractableBase.h"

APaintInteractableBase::APaintInteractableBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void APaintInteractableBase::OnBluePaintApplied_Implementation()
{
}

void APaintInteractableBase::OnYellowPaintApplied_Implementation()
{
}

void APaintInteractableBase::OnGreenPaintApplied_Implementation()
{
}
