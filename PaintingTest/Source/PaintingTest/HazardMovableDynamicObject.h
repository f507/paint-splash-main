// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MovableDynamicWorldObjects.h"
#include "HazardMovableDynamicObject.generated.h"

/**
 * 
 */
UCLASS()
class PAINTINGTEST_API AHazardMovableDynamicObject : public AMovableDynamicWorldObjects
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AHazardMovableDynamicObject();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player Threat")
	bool m_bIsAThreat = true;

};
