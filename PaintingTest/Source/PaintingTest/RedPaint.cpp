// Fill out your copyright notice in the Description page of Project Settings.


#include "RedPaint.h"

URedPaint::URedPaint()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

void URedPaint::BeginPlay()
{
	Super::BeginPlay();

	
}

void URedPaint::ResetColorAttrs()
{
	Super::ResetColorAttrs();
}

void URedPaint::ApplyPaintAttrs()
{
	Super::ApplyPaintAttrs();
}

void URedPaint::OnColorMix()
{
}

void URedPaint::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}
