// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MiniGameBase.generated.h"

UCLASS()
class PAINTINGTEST_API AMiniGameBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMiniGameBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game State")
	bool m_IsActive = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Properties")
	int32 m_CurrentScore = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Properties")
	float m_MiniGameTime = 2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Properties")
	uint8 m_NumOfRounds = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Properties")
	int32 m_TargetScore = 10;

	//TODO create more complex game activation logic, for now pressure plates will be used to activate
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Game Activation")
	TArray<AActor*> m_PlatesToActivate;

	UPROPERTY()
	float m_CurrentRoundTimer = 0;
	UPROPERTY()
	uint8 m_RoundsLeft = 1;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void StartGame();

	virtual void AddToScore(int32 points);
	virtual inline  bool IsRoundOver() { return m_CurrentRoundTimer <= 0; }
	virtual inline bool IsMiniGameOver() { return m_RoundsLeft == 0; }
};
