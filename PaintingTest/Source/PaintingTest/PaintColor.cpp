// Fill out your copyright notice in the Description page of Project Settings.


#include "PaintColor.h"

// Sets default values for this component's properties
UPaintColor::UPaintColor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UPaintColor::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UPaintColor::ResetColorAttrs()
{
	b_ColorApplied = false;
}

void UPaintColor::ApplyPaintAttrs()
{
	b_ColorApplied = true;
}

void UPaintColor::OnColorMix()
{
}


// Called every frame
void UPaintColor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

