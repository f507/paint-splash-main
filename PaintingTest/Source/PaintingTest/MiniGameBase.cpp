// Fill out your copyright notice in the Description page of Project Settings.


#include "MiniGameBase.h"

// Sets default values
AMiniGameBase::AMiniGameBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMiniGameBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMiniGameBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMiniGameBase::StartGame()
{
	m_IsActive = true;
}

void AMiniGameBase::AddToScore(int32 points)
{
	m_CurrentScore = m_IsActive ? m_CurrentScore + points : m_CurrentScore;
}

