// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaintColor.h"
#include "GreenPaint.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PAINTINGTEST_API UGreenPaint : public UPaintColor
{
	GENERATED_BODY()
	
public:
	UGreenPaint();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	//Green Functions
	UFUNCTION()
	void ScaleFromPaintApplication(float ScaleRate);
	UFUNCTION()
	void InitializePaintScaling(float DeltaTime);
	

public:
	//Methods to be overiden per each color
	UFUNCTION()
	void ResetColorAttrs() override;
	UFUNCTION()
	void ApplyPaintAttrs() override;
	UFUNCTION()
	void OnColorMix() override;
	
private:
	void UpdateFMODVariables();
public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:

	//Applied Green Properties
	UPROPERTY(EditAnywhere, Category = "Green Scaling")
	FVector m_MinScale = FVector::OneVector;
	UPROPERTY(EditAnywhere, Category = "Green Scaling")
	FVector m_MaxScale = FVector::OneVector;
	UPROPERTY(EditAnywhere, Category = "Green Scaling")
	float m_ScaleToMaxTime = 2.f;

	UPROPERTY()
	FVector m_ActorMaxScale = FVector::ZeroVector;
	UPROPERTY()
	FVector m_InitialActorScale = FVector::ZeroVector;
	UPROPERTY()
	float m_CurrentScaleAlpha = 0;
	UPROPERTY()
	float m_ScalePercentRate = 0;

	UPROPERTY()
	float m_AlphaGrowingDirection = 1.f;
	UPROPERTY(BlueprintReadOnly)
		bool m_Growing = false;
	UPROPERTY(BlueprintReadOnly)
		bool m_Shrinking = false;

};
