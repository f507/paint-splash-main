// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MovableDynamicWorldObjects.generated.h"


UENUM()
enum ColorsApplied
{
	NONE,
	WHITE	   UMETA(DisplayName = "White"),

	RED        UMETA(DisplayName = "Red"),
	YELLOW	   UMETA(DisplayName = "Yellow"),
	BLUE	   UMETA(DisplayName = "Blue"),

	ORANGE	   UMETA(DisplayName = "Orange"),
	GREEN	   UMETA(DisplayName = "Green"),
	PURPLE	   UMETA(DisplayName = "Purple")
};


USTRUCT()
struct FDynamicMatRefs
{
	GENERATED_USTRUCT_BODY()

	UMaterial* Yellow = nullptr;
	UMaterial* Blue = nullptr;
	UMaterial* Green = nullptr;
	UMaterial* White = nullptr;
};

UCLASS()
class PAINTINGTEST_API AMovableDynamicWorldObjects : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY()
	USceneComponent* m_Root = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* m_MeshComp = nullptr;
	/*UPROPERTY(EditAnywhere)
	class UBoxComponent* m_BoxComp = nullptr;*/

public:	
	// Sets default values for this actor's properties
	AMovableDynamicWorldObjects();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "Dynamic Flags")
	bool m_bIsPaintable = true;
	UPROPERTY()
	bool m_IsStationary = false;

	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly , Category = "Color")
	TEnumAsByte<ColorsApplied> m_ColorApplied;
	UPROPERTY()
	TEnumAsByte<ColorsApplied> m_PreviousColorApplied;
	UPROPERTY()
	TEnumAsByte<ColorsApplied> m_LastColorApplied;

	UPROPERTY()
	UMaterial* m_PaintableMaterial = nullptr;
	UPROPERTY()
	UParticleSystem* m_ps = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UParticleSystemComponent* m_psComp = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Color")
	float m_PaintAppDelay_InSec = 1.f;
	UPROPERTY()
	float m_PaintDelayHandler = 0.f;

	//Painted Dynamic Material Refs 
	UPROPERTY()
	FDynamicMatRefs m_DynamicMaterials;

protected:

	UFUNCTION()
	virtual void OnColorChange();
	UFUNCTION()
	void ResetActorToDefaultState();

	UFUNCTION(BlueprintCallable)
	virtual void OnPaintedGreen();
	UFUNCTION(BlueprintCallable)
	virtual void OnPaintedYellow();
	UFUNCTION(BlueprintCallable)
	virtual void OnPaintedBlue();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	//TESTING PURPOSES ONLY
	UFUNCTION(BlueprintCallable)
	virtual void SetColorApplied(uint8 colorID);

	UFUNCTION(BlueprintCallable)
	virtual void SwapMaterials(UMaterial* newMaterial, uint8 ColorID);

///Color Components
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UYellowPaint* m_YellowPaintComp = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UGreenPaint* m_GreenPaintComp = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UBluePaint* m_BluePaintComp = nullptr;
	UPROPERTY(EditAnywhere)
	class URedPaint* m_RedPaintComp = nullptr;
};
