// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaintColor.h"
#include "YellowPaint.generated.h"

/**
 *
 */

UENUM()
enum YellowMobility
{
	MOTION     UMETA(DisplayName = "Follows Preset Path"),
	ANIMATION  UMETA(DisplayName = "Animated")
};

UENUM()
enum TraversalType
{
	LOOP		UMETA(DisplayName = "Loop"),
	PINGPONG	UMETA(DisplayName = "Ping Pong")
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PAINTINGTEST_API UYellowPaint : public UPaintColor
{
	GENERATED_BODY()

public:
	UYellowPaint();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	//Yellow Functions
	UFUNCTION()
		virtual bool ValidDistance(FVector& a, FVector& b, float distanceToCheck);
	UFUNCTION()
		void CheckForNextTargetDestination(float delta);
	UFUNCTION(BlueprintCallable)
		bool IsPlatformMoving();
	UFUNCTION(BlueprintCallable)
		float GetMovementSpeed();
	UFUNCTION(BlueprintCallable)
		bool IsObjectYellow();

public:
	//Methods to be overiden per each color
	UFUNCTION()
		virtual void ResetColorAttrs() override;
	UFUNCTION()
		virtual void ApplyPaintAttrs() override;
	UFUNCTION()
		virtual void OnColorMix() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	UPROPERTY(EditAnywhere, Category = "Target Destinations")
		TArray<AActor*> m_TargetLocations;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Yellow Mobility")
		TEnumAsByte<YellowMobility> m_YellowTypeApplication;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Yellow Mobility")
		TEnumAsByte<TraversalType> m_TraversalType;

	UPROPERTY()
		int32 m_CurrentTarget = 0;

	//Applied Yellow Properties
	UPROPERTY(EditAnywhere, Category = "Yellow Mobility")
		float m_Speed = 125.f;
	UPROPERTY(EditAnywhere, Category = "Yellow Mobility")
		float m_SuccessfulDistance = 10.f;
	UPROPERTY(EditAnywhere, Category = "Yellow Mobility")
		float m_WaitTimer = 2.f;
	
	float m_remainingTime = 0.f;

	UPROPERTY()
		int32 m_TargetRate = 1;
};
