// Fill out your copyright notice in the Description page of Project Settings.


#include "YellowPaint.h"

UYellowPaint::UYellowPaint()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
   // off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}
void UYellowPaint::BeginPlay()
{
	Super::BeginPlay();

	//Setup owning paintable actor if any
	m_OwningPaintableActor = GetOwner();

	m_TargetLocations.Insert(m_OwningPaintableActor, 0);
	m_CurrentTarget = 1;
}

void UYellowPaint::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//Don't tick if the same colored paint was not applied to the owning actor
	if (!b_ColorApplied) return;

	CheckForNextTargetDestination(DeltaTime);

}

void UYellowPaint::ResetColorAttrs()
{
	//Temporary reset TODO reset the actor back to initial state before paint was applied
	Super::ResetColorAttrs();
}

void UYellowPaint::ApplyPaintAttrs()
{
	Super::ApplyPaintAttrs();
}

void UYellowPaint::OnColorMix()
{
}

bool UYellowPaint::ValidDistance(FVector& a, FVector& b, float distanceToCheck)
{

	return FVector::Distance(a, b) <= distanceToCheck;
}

void UYellowPaint::CheckForNextTargetDestination(float delta)
{
	//Targt check only occurs if target listing has occured
	if (m_TargetLocations.Num() <= 1 || !m_OwningPaintableActor) return; //Animation goes here

	if (m_remainingTime > 0) {
		m_remainingTime -= delta;
		return;
	}

	int32 numOfLocations = m_TargetLocations.Num();

	FVector CurrentLocation = m_OwningPaintableActor->GetActorLocation();
	FVector CurrentTarget = m_TargetLocations[m_CurrentTarget]->GetActorLocation();
	
	bool shouldMoveToNextTarget = ValidDistance(CurrentTarget, CurrentLocation, m_SuccessfulDistance);

	if (shouldMoveToNextTarget)
	{
		m_CurrentTarget += m_TargetRate;
		m_remainingTime = m_WaitTimer;
	}

	if (m_CurrentTarget > numOfLocations - 1 || m_CurrentTarget == 0)
	{
		if (m_TraversalType == TraversalType::PINGPONG)
		{
			m_TargetRate *= -1;
			m_CurrentTarget += m_TargetRate;
		}
		else
		{
			bool hasReachedEnd = m_CurrentTarget > numOfLocations - 1;
			m_CurrentTarget = hasReachedEnd? 0 : m_CurrentTarget;
		}
		
	}

	CurrentTarget = m_TargetLocations[m_CurrentTarget]->GetActorLocation();

	FVector TargetDirection = (CurrentTarget - CurrentLocation).GetSafeNormal();

	m_OwningPaintableActor->SetActorLocation(CurrentLocation + TargetDirection * m_Speed * delta);
}

bool UYellowPaint::IsPlatformMoving() {
	return m_remainingTime <= 0 && b_ColorApplied;
}

bool UYellowPaint::IsObjectYellow() {
	return b_ColorApplied;
}

float UYellowPaint::GetMovementSpeed() {
	return m_Speed;
}