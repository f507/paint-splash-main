// Copyright Epic Games, Inc. All Rights Reserved.

#include "PaintingTest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PaintingTest, "PaintingTest" );

DEFINE_LOG_CATEGORY(LogPaintingTest)
 