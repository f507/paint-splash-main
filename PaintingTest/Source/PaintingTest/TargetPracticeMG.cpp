// Fill out your copyright notice in the Description page of Project Settings.


#include "TargetPracticeMG.h"

ATargetPracticeMG::ATargetPracticeMG()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ATargetPracticeMG::BeginPlay()
{
	Super::BeginPlay();
}

void ATargetPracticeMG::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
