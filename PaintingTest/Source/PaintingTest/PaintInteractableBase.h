// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MovableDynamicWorldObjects.h"
#include "PaintInteractableBase.generated.h"

/**
 * 
 */
UCLASS()
class PAINTINGTEST_API APaintInteractableBase : public AMovableDynamicWorldObjects
{
	GENERATED_BODY()
	
public:
	APaintInteractableBase();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Painting Events")
		void OnBluePaintApplied();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Painting Events")
		void OnYellowPaintApplied();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Painting Events")
		void OnGreenPaintApplied();
};
