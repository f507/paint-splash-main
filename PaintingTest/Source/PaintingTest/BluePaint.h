// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaintColor.h"
#include "BluePaint.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PAINTINGTEST_API UBluePaint : public UPaintColor
{
	GENERATED_BODY()
	
public:
	UBluePaint();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	//Yellow Functions
	UFUNCTION()
	void SwitchCollisionPreset();

public:
	//Methods to be overiden per each color
	UFUNCTION()
	virtual void ResetColorAttrs() override;
	UFUNCTION()
	virtual void ApplyPaintAttrs() override;
	UFUNCTION()
	virtual void OnColorMix() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:

	UPROPERTY()
	UStaticMeshComponent* m_ActorMesh = nullptr;
	UPROPERTY()
	FName m_DefaultCollisionPreset;

	UPROPERTY(EditAnywhere, Category = "Overlap Properties")
	float m_OverlapTime = 2.f;
	UPROPERTY()
	float m_CurrentOverlapTimer;
	UPROPERTY()
	float m_DeltaTime;
};
