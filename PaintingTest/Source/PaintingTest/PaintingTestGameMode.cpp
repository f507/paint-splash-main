// Copyright Epic Games, Inc. All Rights Reserved.

#include "PaintingTestGameMode.h"
#include "PaintingTestPlayerController.h"
#include "PaintingTestCharacter.h"
#include "UObject/ConstructorHelpers.h"

APaintingTestGameMode::APaintingTestGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = APaintingTestPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}