// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaintColor.h"
#include "RedPaint.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PAINTINGTEST_API URedPaint : public UPaintColor
{
	GENERATED_BODY()
public:
	URedPaint();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	//Methods to be overiden per each color
	UFUNCTION()
		virtual void ResetColorAttrs() override;
	UFUNCTION()
		virtual void ApplyPaintAttrs() override;
	UFUNCTION()
		virtual void OnColorMix() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

};
