// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MovableDynamicWorldObjects.h"
#include "InteractivePaintableObjects.generated.h"

/**
 * 
 */
UCLASS()
class PAINTINGTEST_API AInteractivePaintableObjects : public AMovableDynamicWorldObjects
{
	GENERATED_BODY()
	
};
