// Fill out your copyright notice in the Description page of Project Settings.


#include "HazardMovableDynamicObject.h"

#include "Engine.h"

#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "Particles/ParticleSystem.h"

//Color Comps
#include "PaintColor.h"
#include "YellowPaint.h"
#include "GreenPaint.h"
#include "BluePaint.h"
#include "RedPaint.h"

AHazardMovableDynamicObject::AHazardMovableDynamicObject()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_RedPaintComp = CreateDefaultSubobject<URedPaint>(TEXT("Red Component"));
}

void AHazardMovableDynamicObject::BeginPlay()
{
	Super::BeginPlay();
	m_ColorApplied = ColorsApplied::RED;
	m_PreviousColorApplied = ColorsApplied::RED;
	m_LastColorApplied = ColorsApplied::RED;
}

void AHazardMovableDynamicObject::Tick(float DeltaTime)
{
	//Only non-stationary actors need to Tick
	if (!m_bIsPaintable) return;

	bool b_delayDone = UGameplayStatics::GetTimeSeconds(GetWorld()) >= m_PaintDelayHandler;

	if (m_ColorApplied != ColorsApplied::NONE && b_delayDone)
	{
		if (m_RedPaintComp)
		{
			if (m_ColorApplied == ColorsApplied::RED)
			{
				m_RedPaintComp->ApplyPaintAttrs();
			}
		}

		//TODO Orange and purple aplications go here
	}

	//TODO this is a temporary solution, ideally threat levels are measure beyond just being painted red
	m_bIsAThreat = m_ColorApplied == ColorsApplied::RED;
}
