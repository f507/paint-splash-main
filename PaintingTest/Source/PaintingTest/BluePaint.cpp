// Fill out your copyright notice in the Description page of Project Settings.


#include "BluePaint.h"

#include "Kismet/GameplayStatics.h"

UBluePaint::UBluePaint()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}
void UBluePaint::BeginPlay()
{
	Super::BeginPlay();

	//Setup owning paintable actor if any
	m_OwningPaintableActor = GetOwner();

	if (m_OwningPaintableActor)
	{
		m_ActorMesh = m_OwningPaintableActor->FindComponentByClass<UStaticMeshComponent>();

		m_DefaultCollisionPreset = m_ActorMesh->GetCollisionProfileName();
	}
}

void UBluePaint::SwitchCollisionPreset()
{
	if (!m_ActorMesh) return;

	m_ActorMesh->SetCollisionProfileName(TEXT("OverlapAllDynamic"));
	//m_ActorMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}

void UBluePaint::ResetColorAttrs()
{
	Super::ResetColorAttrs();
	if(m_ActorMesh)
	{
		m_ActorMesh->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	}
	b_ColorApplied = false;
}

void UBluePaint::ApplyPaintAttrs()
{
	Super::ApplyPaintAttrs();

	SwitchCollisionPreset();

	m_CurrentOverlapTimer = UGameplayStatics::GetTimeSeconds(GetWorld()) + m_OverlapTime;
}

void UBluePaint::OnColorMix()
{
}

void UBluePaint::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//Don't tick if the same colored paint was not applied to the owning actor
	if (!b_ColorApplied) return;
	//SwitchCollisionPreset();

	TArray<AActor*> OverlappingActors;
	m_ActorMesh->GetOverlappingActors(OverlappingActors);

	bool OverlapTimerUp = m_CurrentOverlapTimer < UGameplayStatics::GetTimeSeconds(GetWorld());

	if (OverlapTimerUp) {
		//ResetColorAttrs();
	}

}
